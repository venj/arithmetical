//
//  SettingsViewController.swift
//  Arithmetical
//
//  Created by venj on 3/19/18.
//  Copyright © 2018 venj. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    @IBOutlet var questionNumberSegmentedControl: UISegmentedControl!
    @IBOutlet var includeZeroSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        questionNumberSegmentedControl?.selectedSegmentIndex = Configuration.shared.questionCount.rawValue
        includeZeroSwitch?.setOn(Configuration.shared.operandsShouldIncludeZero, animated: false)
    }
}

extension SettingsViewController {
    @IBAction func dismissMe(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func questionNumberSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        Configuration.shared.questionCount = QuestionCount(rawValue: sender.selectedSegmentIndex) ?? .twenty
    }

    @IBAction func includeZeroSwitchValueChanged(_ sender: UISwitch) {
        Configuration.shared.operandsShouldIncludeZero = sender.isOn
    }
}
