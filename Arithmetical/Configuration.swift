//
//  Configuration.swift
//  Haru
//
//  Created by venj on 12/29/17.
//  Copyright © 2017 Sukiapps. All rights reserved.
//

import Foundation

let NumberOfQuestions = "NumberOfQuestionsKey"
let OperandsIncludeZero = "OperandsIncludeZeroKey"

enum QuestionCount: Int {
    case ten = 0
    case fifteen
    case twenty

    var value: Int {
        switch self {
        case .ten:
            return 10
        case .fifteen:
            return 15
        case .twenty:
            return 20
        }
    }
}

open class Configuration {
    public static let shared = Configuration()
    private let defaults = UserDefaults.standard
    private init() {
        defaults.register(defaults: [NumberOfQuestions : QuestionCount.twenty.rawValue,
                                     OperandsIncludeZero: false,
                                     ])
        defaults.synchronize()
    }

    var questionCount: QuestionCount {
        get {
            return QuestionCount(rawValue: defaults.integer(forKey: NumberOfQuestions)) ?? .ten
        }
        set {
            defaults.set(newValue.rawValue, forKey: NumberOfQuestions)
            defaults.synchronize()
        }
    }

    var operandsShouldIncludeZero: Bool {
        get {
            return defaults.bool(forKey: OperandsIncludeZero)
        }
        set {
            defaults.set(newValue, forKey: OperandsIncludeZero)
            defaults.synchronize()
        }
    }

}
