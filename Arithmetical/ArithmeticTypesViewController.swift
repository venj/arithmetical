//
//  ArithmeticTypesViewController.swift
//  Arithmetical
//
//  Created by venj on 3/17/18.
//  Copyright © 2018 venj. All rights reserved.
//

import UIKit

class ArithmeticTypesViewController: UITableViewController {
    let titles = [NSLocalizedString("Arithmetic within 10", comment: "10 以内的运算"),
                  NSLocalizedString("Arithmetic within 20", comment: "20 以内的运算"),
                  ]
    let objects = [[NSLocalizedString("Addition within 10", comment: "10 以内的加法"),
                   NSLocalizedString("Subtraction within 10", comment: "10 以内的减法") ,
                   NSLocalizedString("Add. & Sub. within 10", comment: "10 以内的加减法"),
                   NSLocalizedString("Muti-Add. within 10", comment: "10 以内连加"),
                   NSLocalizedString("Muti-Sub. within 10", comment: "10 以内的连减") ,
                   NSLocalizedString("Mixed Add. Sub. within 10", comment: "10 以内的加减混合运算")],
                   [NSLocalizedString("Addition within 20", comment: "20 以内的加法"),
                   NSLocalizedString("Subtraction within 20", comment: "20 以内的减法") ,
                   NSLocalizedString("Add. & Sub. within 20", comment: "20 以内的加减法"),
                   NSLocalizedString("Muti-Add. within 20", comment: "20 以内连加"),
                   NSLocalizedString("Muti-Sub. within 20", comment: "20 以内的连减") ,
                   NSLocalizedString("Mixed Add. Sub. within 20", comment: "20 以内的加减混合运算")],
                   ]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow, let controller = segue.destination as? QuestionViewController {
                let i = objects[0..<(indexPath.section)].reduce(0, { (r, s) in r + s.count }) + indexPath.row
                controller.type = ArithmeticType(rawValue: i)!
                controller.title = objects[indexPath.section][indexPath.row]

                // Clean up
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects[section].count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titles[section]
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel!.text = objects[indexPath.section][indexPath.row]
        return cell
    }
}

