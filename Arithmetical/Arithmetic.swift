//
//  Arithmetic.swift
//  Arithmetical
//
//  Created by venj on 3/17/18.
//  Copyright © 2018 venj. All rights reserved.
//

import Foundation

enum ArithmeticType: Int {
    case plus_10 = 0
    case minus_10
    case plus_minus_10
    case multiple_plus_10
    case multiple_minus_10
    case mix_plus_minus_10
    case plus_20
    case minus_20
    case plus_minus_20
    case multiple_plus_20
    case multiple_minus_20
    case mix_plus_minus_20
}

enum ArithmeticOperator: Int, CustomStringConvertible {
    case plus = 0
    case minus
    case times
    case devide

    var description: String {
        var opt: String
        switch self {
        case .plus:
            opt = "+"
        case .minus:
            opt = "−"
        case .times:
            opt = "×"
        case .devide:
            opt = "÷"
        }

        return opt
    }
}

extension ArithmeticOperator: Equatable {}

struct SimpleArithmetic {
    var operands: [Int]
    var operators: [ArithmeticOperator]
    var answer: Int

    static func generateEquation(_ type: ArithmeticType, includeZero: Bool) -> SimpleArithmetic {
        switch type {
        case .plus_10:
            return generateAddition(2, limit: 10, includeZero: includeZero)
        case .minus_10:
            return generateSubtraction(2, limit: 10, includeZero: includeZero)
        case .plus_minus_10:
            return generateAdditionOrSubtraction(2, limit: 10, includeZero: includeZero)
        case .multiple_plus_10:
            return generateAddition(3, limit: 10, includeZero: includeZero)
        case .multiple_minus_10:
            return generateSubtraction(3, limit: 10, includeZero: includeZero)
        case .mix_plus_minus_10:
            return generateAdditionOrSubtraction(3, limit: 10, includeZero: includeZero)
        case .plus_20:
            return generateAddition(2, limit: 20, includeZero: includeZero)
        case .minus_20:
            return generateSubtraction(2, limit: 20, includeZero: includeZero)
        case .plus_minus_20:
            return generateAdditionOrSubtraction(2, limit: 20, includeZero: includeZero)
        case .multiple_plus_20:
            return generateAddition(3, limit: 20, includeZero: includeZero)
        case .multiple_minus_20:
            return generateSubtraction(3, limit: 20, includeZero: includeZero)
        case .mix_plus_minus_20:
            return generateAdditionOrSubtraction(3, limit: 20, includeZero: includeZero)
        }
    }

    var question: String {
        var result = ""
        operands.enumerated().forEach({ (args) in
            let (index, number) = args
            if index == 0 {
                result = "\(number)"
            }
            else {
                let raw = operators[(index - 1)].rawValue
                if raw == 0 {
                    result += " + \(number) "
                }
                else if raw == 1 {
                    result += " - \(number) "
                }
            }
        })

        result += " = "

        return result
    }

    // Mutiple addition
    static func generateAddition(_ numberOfOperands: Int, limit: Int, includeZero: Bool) -> SimpleArithmetic {
        if numberOfOperands < 2 { fatalError("Operands should not less than 2!") }
        var ari = SimpleArithmetic(operands: [0, 0], operators:[.plus], answer: 0)
        while true {
            let operands: [Int] = (0..<numberOfOperands).map { _ in getRandom(limit, includeZero: includeZero) }
            let operators: [ArithmeticOperator] = (0..<(numberOfOperands - 1)).map { _ in .plus }
            let answer = operands.reduce(0, +)
            if answer <= limit {
                // Reject operands with 2 more same number randomly. 75% less than normal.
                if Set<Int>(operands).count < numberOfOperands && getRandom(4) != 1 {
                    continue
                }

                ari = SimpleArithmetic(operands: operands, operators: operators, answer: answer)
                break
            }
        }
        return ari
    }

    // Mutiple subtraction
    static func generateSubtraction(_ numberOfOperands: Int, limit: Int, includeZero: Bool) -> SimpleArithmetic {
        var ari = SimpleArithmetic(operands: [0, 0], operators: [.minus], answer: 0)
        while true {
            let operands: [Int] = (0..<numberOfOperands).map { _ in getRandom(limit, includeZero: includeZero) }
            let operators: [ArithmeticOperator] = (0..<(numberOfOperands - 1)).map { _ in .minus }

            var answer = 0
            var isValid = true
            operands.enumerated().forEach({ (args) in
                let (index, number) = args
                if index == 0 {
                    answer = number
                }
                else {
                    answer -= number
                }
                if answer <= 0 {
                    isValid = false
                }
            })

            if !isValid { continue }

            if answer >= 0 {
                // Reject operands with 2 more same number randomly. 75% less than normal.
                if Set<Int>(operands).count < numberOfOperands && getRandom(4) != 1 {
                    continue
                }

                ari = SimpleArithmetic(operands: operands, operators: operators, answer: answer)
                break
            }
        }
        return ari
    }

    static func generateAdditionOrSubtraction(_ numberOfOperands: Int, limit: Int, includeZero: Bool = false) -> SimpleArithmetic {
        var ari = SimpleArithmetic(operands: [0, 0], operators: [.plus], answer: 0)

        while true {
            let operators: [ArithmeticOperator] = (0..<(numberOfOperands - 1)).map { _ in
                let opts: [ArithmeticOperator] = [.plus, .minus]
                return opts[getRandom(1, includeZero: true)]
            }
            let operands: [Int] = (0..<numberOfOperands).map { _ in getRandom(limit, includeZero: includeZero) }
            var answer = 0
            var isValid = true
            operands.enumerated().forEach({ (args) in
                let (index, number) = args
                if index == 0 {
                    answer = number
                }
                else {
                    let raw = operators[(index - 1)].rawValue
                    if raw == 0 {
                        answer += number
                    }
                    else if raw == 1 {
                        answer -= number
                    }
                }
                if answer < 0 || answer > limit {
                    isValid = false
                }
            })

            if !isValid { continue }

            if answer >= 0 && answer <= limit {
                // Reject operands with 2 more same number randomly. 75% less than normal.
                if Set<Int>(operands).count < numberOfOperands && getRandom(4) != 1 {
                    continue
                }

                ari = SimpleArithmetic(operands: operands, operators: operators, answer: answer)
                break
            }
        }
        return ari
    }

    static func getRandom(_ number: Int, includeZero: Bool = false) -> Int {
        if includeZero {
            let base = number + 1
            return Int(arc4random_uniform(UInt32(base)))
        }
        else {
            return Int(arc4random_uniform(UInt32(number))) + 1
        }
    }
}

extension SimpleArithmetic: CustomStringConvertible {
    var description: String {
        return "\(question)\(answer)"
    }
}

extension SimpleArithmetic: Equatable {
    static func ==(lhs: SimpleArithmetic, rhs: SimpleArithmetic) -> Bool {
        return lhs.operands == rhs.operands && lhs.operators == rhs.operators
    }
}
