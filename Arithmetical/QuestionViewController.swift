//
//  QuestionViewController.swift
//  Arithmetical
//
//  Created by venj on 3/17/18.
//  Copyright © 2018 venj. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {

    let totalQuestions: Int = Configuration.shared.questionCount.value
    let showAnswersSegue = "ShowAnswersViewController"

    @IBOutlet var timerLabel: UILabel!
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var progressLabel: UILabel!
    @IBOutlet var resultField: UITextField!
    @IBOutlet var nextButton: UIButton!

    var currentQuestion: Int = 0
    var equations: [SimpleArithmetic] = []
    var seconds: Int = 0 {
        didSet {
            DispatchQueue.main.async {
                self.timerLabel.text = self.seconds.minutes
                if self.seconds > self.totalQuestions * 5 {
                    self.timerLabel.textColor = .red
                }
            }
        }
    }

    var timer: Timer? = nil
    var answers: [Int] = []
    var type: ArithmeticType!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        equations = generateEquations(totalQuestions, includeZero: Configuration.shared.operandsShouldIncludeZero)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resultField?.becomeFirstResponder()
        questionLabel?.text = equations[currentQuestion].question
        progressLabel?.text = "\(currentQuestion + 1)/\(totalQuestions)"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer(_:)), userInfo: nil, repeats: true)
            timer?.fire()
        }
    }

    deinit {
        timer?.invalidate()
        timer = nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func nextQuestion(_ sender: Any?) {
        currentQuestion += 1

        if answers.count < equations.count {
            answers.append(Int(resultField.text ?? "0", radix: 10) ?? 0)
        }

        if currentQuestion >= equations.count {
            // Show result view controller
            resultField.resignFirstResponder()
            timer?.invalidate()
            performSegue(withIdentifier: showAnswersSegue, sender: sender)
        }
        else {
            if currentQuestion == equations.count - 1 { // last question
                nextButton.setTitle("☑️", for: .normal)
            }
            questionLabel.text = equations[currentQuestion].question
            resultField.text = ""
            nextButton.isHidden = true
            progressLabel.text = "\(currentQuestion + 1)/\(totalQuestions)"
        }
    }

    @objc
    func updateTimer(_ timer: Timer) {
        seconds += 1
    }
}

extension QuestionViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showAnswersSegue, let vc = segue.destination as? AnswersViewController {
            vc.answers = answers
            vc.questions = equations
            vc.totalTime = seconds
            // Reset values
            currentQuestion = 0
        }
    }
}

extension QuestionViewController {
    func generateEquations(_ numbers: Int, includeZero: Bool) -> [SimpleArithmetic] {
        var equations: [SimpleArithmetic] = []

        (0..<numbers).forEach { _ in
            while true {
                let equ = SimpleArithmetic.generateEquation(type, includeZero: includeZero)
                if equations.contains(equ) {
                    continue
                }
                else {
                    equations.append(equ)
                    break
                }
            }
        }

        return equations
    }
}

extension QuestionViewController {
    @IBAction func textDidChange(_ textField: UITextField) {
        if textField.text != "" {
            nextButton.isHidden = false
        }
        else {
            nextButton.isHidden = true
        }
    }
}
