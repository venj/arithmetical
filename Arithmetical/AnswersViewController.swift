//
//  AnswersViewController.swift
//  Arithmetical
//
//  Created by venj on 3/17/18.
//  Copyright © 2018 venj. All rights reserved.
//

import UIKit

class AnswersViewController: UITableViewController {

    var questions: [SimpleArithmetic] = []
    var timeLimit: Int {
        return questions.map { $0.operators.count * 5 }.reduce(0, +)
    }
    var answers: [Int] = []
    var totalTime: Int = 0

    @IBOutlet var totalQuestionsLabel: UILabel!
    @IBOutlet var rightQuestionsLabel: UILabel!
    @IBOutlet var rightPercentLabel: UILabel!
    @IBOutlet var totalTimeLabel: UILabel!
    @IBOutlet var averageTimeLabel: UILabel!
    @IBOutlet var rateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("Answers", comment: "结果")

        // Remove Back Button
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action:nil)

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(finishTask(_:)))
        navigationItem.rightBarButtonItem = doneButton
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let totalCount = questions.count
        totalTimeLabel.text = "\(totalTime.minutes)"
        averageTimeLabel.text = String(format:"%.1f", Double(totalTime) / Double(totalCount))
        totalQuestionsLabel.text = "\(questions.count)"
        let rightCount = questions.enumerated().filter { arg in
            let (id, quest) = arg
            return quest.answer == answers[id]
        }.count
        rightQuestionsLabel.text = "\(rightCount)"
        let rightPercent = Int(Double(rightCount) / Double(totalCount) * 100.0)
        rightPercentLabel.text = "\(rightPercent)%"

        if rightPercent == 100 && totalTime <= timeLimit {
            rateLabel.text = "⭐️⭐️⭐️"
        }
        else if rightPercent >= 90 && totalTime <= timeLimit * 2 {
            rateLabel.text = "⭐️⭐️"
        }
        else if rightPercent > 70 {
            rateLabel.text = "⭐️"
        }
        else {
            rateLabel.text = "☹️"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func finishTask(_ sender: Any?) {
        navigationController?.popToRootViewController(animated: true)
    }

    @objc func empty(_ sender: Any?) {}

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as! AnswerViewCell
        let question = questions[indexPath.row]
        let answer = answers[indexPath.row]
        let isCurrect = question.answer == answer

        cell.questionLabel.text = "\(question.question) \(answer)"
        cell.questionLabel.textColor = isCurrect ? .darkGreen : .red

        if !isCurrect {
            cell.rightAnswerLabel.text = String(format: NSLocalizedString("Answer: %ld", comment: "正确答案: %ld"), question.answer)
            cell.checkMarkLabel.text = "✗"
            cell.checkMarkLabel.textColor = .red
        }
        else {
            cell.rightAnswerLabel.text = ""
            cell.checkMarkLabel.text = "✓"
            cell.checkMarkLabel.textColor = .darkGreen
        }

        return cell
    }
}
