//
//  Extensions.swift
//  Arithmetical
//
//  Created by venj on 3/17/18.
//  Copyright © 2018 venj. All rights reserved.
//

import UIKit

extension Int {
    var minutes: String {
        if self < 60 {
            return String(format: "0:%02ld", self)
        }
        else {
            let minutes = self / 60
            let remain = self % 60
            return String(format: "%ld:%02ld", minutes, remain)
        }
    }
}

extension UIColor {
    static var darkGreen: UIColor {
        return UIColor(red:0.40, green:0.73, blue:0.16, alpha:1.00)
    }
}
