//
//  AnswerViewCell.swift
//  Arithmetical
//
//  Created by venj on 3/17/18.
//  Copyright © 2018 venj. All rights reserved.
//

import UIKit

class AnswerViewCell: UITableViewCell {

    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var rightAnswerLabel: UILabel!
    @IBOutlet var checkMarkLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
